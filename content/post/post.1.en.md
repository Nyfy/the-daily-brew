+++
title = 'A Delicate Balance: Guatemalan Huehuetenango'
slug = 'post1'
image = 'images/post1.jpg'
date = "2019-08-05T04:05:22" 
description = 'A fable about the Guatemalan Huehuetenango roasted by Happy Goat Coffee Company'
disableComments = true
+++
As the long August weekend swiftly approached, I found myself entranced by the spacious and rustic atmosphere of the Happy Goat Coffee Company on Rideau street. This homespun coffee bar offers much more than just crisp, fresh grinds to the local clientele. It offers a welcome seat in an otherwise busy locale, and fresh single origin roasts for homebrewed comfort.

During this particular venture, I picked up a dozen ounces of the Guatemala Huehuetenango. A worthy choice, as I later found it was voted best drip coffee at Ottawa Coffee Fest 2019. The bag itself had the traditional - albeit somewhat unsettling - goat logo indicative of Happy Goat coffees. The packaging was refreshingly informative, and included not only the origin details and roast level, but even specifically recommended a ratio of and water temperature for an optimal infusion. Unfortunately no recommended grind size was provided, so my first few efforts were naturally somewhat hit or miss on the extraction.

Grinding the beans throughout the long weekend never failed to blanket the room in a floral haze. The coffee bloom was hearty and persistent, and produced many delectable aromas during the brews. The beans themselves danced a delicate balance between light and medium roasts, foreshadowing the yin and yang of its flavour profile.

Once allowed to cool, the Guatemalan coffee delivered strong floral notes overlapped by chocolate and nutty earth. At the crest sits a touch of sweetness, molasses almost reminiscent of creme brûlée. As the warm embrace fades only cocoa and almonds remains, a simple echo of what once was. This roast strikes a careful balance between two worlds of flavour, empowering it’s embiber with the inspiration needed to get through another day.