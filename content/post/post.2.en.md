+++
title = 'Misty City Night'
slug = 'post2'
image = 'images/post2.jpg'
date = "2019-08-10T11:48:22" 
description = 'A poem inspired by the Ethiopian Yirgacheffe roasted by Happy Goat Coffee Company'
disableComments = true
+++
Misty city night  
all of time ahead of me  
yet so much behind.

Cricket's calming song  
under darkness, pensive mind  
people without names.

Trumpet fills the void  
softening the air, velvet  
worries fade to thought.

The stars shine down  
on many beautiful people  
who rarely look up.

Dew on the park grass  
a million blades rest  
content, cool again.

In stone, braver men  
never to be forgotten  
seldom remembered.

Trumpet fades away  
inevitable as always  
sirens of sleep sing.

Cricket's summer song  
all people wish for more time  
when the sun is gone.

Misty city night  
all of time ahead of me  
yet so much behind.